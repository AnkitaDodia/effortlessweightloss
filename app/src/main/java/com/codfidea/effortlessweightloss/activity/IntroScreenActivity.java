package com.codfidea.effortlessweightloss.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.adapter.SliderAdapter;
import com.codfidea.effortlessweightloss.common.BaseActivity;

import java.util.ArrayList;

public class IntroScreenActivity extends BaseActivity {

    ViewPager viewPager;

    TabLayout indicator;

    ArrayList<Integer> image;
    ArrayList<String> text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_screen);

        initView();
        setData();
    }

    private void initView() {
        viewPager = findViewById(R.id.viewPager);

        indicator = findViewById(R.id.indicator);

//        text_next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(IntroScreenActivity.this,DashBoardActivity.class));
//            }
//        });
    }

    private void setData() {
        image = new ArrayList<>();
        image.add(R.drawable.intro_slider_img_1);
        image.add(R.drawable.intro_slider_img_2);
        image.add(R.raw.svakriti);
        image.add(R.drawable.intro_slider_img_3);

        text = new ArrayList<>();//"SvAkRiThI\n\n" +
        text.add("Six Element Approach to Live a Healthy Life");
        text.add("A place where you can achieve your health goal to transform both your body and mind");
        text.add("");
        text.add("Now, are you ready to transform your life?");

        viewPager.setAdapter(new SliderAdapter(IntroScreenActivity.this, image, text));
        indicator.setupWithViewPager(viewPager, true);
    }
}
