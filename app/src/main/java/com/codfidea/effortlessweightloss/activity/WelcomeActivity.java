package com.codfidea.effortlessweightloss.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.codfidea.effortlessweightloss.R;

public class WelcomeActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3500;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mContext = this;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                startActivity(new Intent(mContext, IntroScreenActivity.class));
                finish();

            }
        }, SPLASH_TIME_OUT);
    }
}
