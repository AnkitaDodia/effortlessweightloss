package com.codfidea.effortlessweightloss.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.common.BaseActivity;

public class HealthHistoryActivity extends BaseActivity {

    HealthHistoryActivity mContext;
    private Toolbar mToolbar;
    TextView txt_label_history, txt_health_form_men, txt_health_form_women, txt_health_form_senior, txt_health_form_children;
    CardView cv_form_men, cv_form_women, cv_form_senior, cv_form_children;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_history);

        mContext = this;

        setUpToolbar();
        initView();
    }

    private void setUpToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.getNavigationIcon().setColorFilter(getResources().getColor( R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setTitle("Health history");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
    }

    private void initView() {

        txt_label_history = findViewById(R.id.txt_label_history);
        txt_label_history.setTypeface(getMediumFonts(mContext));

        txt_health_form_men = findViewById(R.id.txt_health_form_men);
        txt_health_form_women = findViewById(R.id.txt_health_form_women);
        txt_health_form_senior = findViewById(R.id.txt_health_form_senior);
        txt_health_form_children = findViewById(R.id.txt_health_form_children);

        txt_health_form_men.setTypeface(getRegularFonts(mContext));
        txt_health_form_women.setTypeface(getRegularFonts(mContext));
        txt_health_form_senior.setTypeface(getRegularFonts(mContext));
        txt_health_form_children.setTypeface(getRegularFonts(mContext));

        cv_form_men = findViewById(R.id.cv_form_men);
        cv_form_women = findViewById(R.id.cv_form_women);
        cv_form_senior = findViewById(R.id.cv_form_senior);
        cv_form_children = findViewById(R.id.cv_form_children);

        cv_form_men.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, WebFormActivity.class));
            }
        });

        cv_form_women.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, WebFormActivity.class));
            }
        });

        cv_form_senior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, WebFormActivity.class));
            }
        });

        cv_form_children.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, WebFormActivity.class));
            }
        });

    }


}
