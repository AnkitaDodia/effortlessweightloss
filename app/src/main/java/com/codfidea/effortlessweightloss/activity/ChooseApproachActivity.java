package com.codfidea.effortlessweightloss.activity;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.adapter.ApproachAdapter;
import com.codfidea.effortlessweightloss.common.BaseActivity;

import java.util.ArrayList;

public class ChooseApproachActivity extends BaseActivity {

    ChooseApproachActivity mContext;
    private Toolbar mToolbar;
    RecyclerView rv_approach_list;
    ArrayList<String> mStringItems = new ArrayList<>();
    ArrayList<Integer> mImagesItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_approach);

        mContext = this;

        setUpToolbar();
        initView();
        setUpRecyclerViewData();
    }

    private void setUpToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.getNavigationIcon().setColorFilter(getResources().getColor( R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setTitle("Choose Approach");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
    }

    private void initView() {
        rv_approach_list = findViewById(R.id.rv_approach_list);
    }

    private void setUpRecyclerViewData(){

        mStringItems.add("Diet");
        mStringItems.add("Physical health");
        mStringItems.add("Mental health");
        mStringItems.add("Genetics");
        mStringItems.add("Environment");
        mStringItems.add("Belief");

        mImagesItems.add(R.drawable.ic_diet);
        mImagesItems.add(R.drawable.ic_physical_health);
        mImagesItems.add(R.drawable.ic_mental_health);
        mImagesItems.add(R.drawable.ic_genetics);
        mImagesItems.add(R.drawable.ic_environmet);
        mImagesItems.add(R.drawable.ic_belief);

        rv_approach_list.setHasFixedSize(true);
        rv_approach_list.setLayoutManager(new GridLayoutManager(mContext, 2));

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_approach_list.addItemDecoration(itemDecorator);

        ApproachAdapter mApproachAdapter = new ApproachAdapter(mContext, mStringItems, mImagesItems);
        rv_approach_list.setAdapter(mApproachAdapter);
    }

}
