package com.codfidea.effortlessweightloss.activity;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.adapter.ChooseHabitAdapter;
import com.codfidea.effortlessweightloss.common.BaseActivity;

import java.util.ArrayList;

public class ChooseHabitActivity extends BaseActivity {

    ChooseHabitActivity mContext;

    private Toolbar mToolbar;

    RecyclerView rv_list;

    ArrayList<String> mHabitItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_goal);

        mContext = this;

        setUpToolbar();
        initView();
        setUpRecyclerViewData();
    }

    private void setUpToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setTitle("Choose Your Habit");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
    }

    private void initView() {
        rv_list = findViewById(R.id.rv_list);
    }

    private void setUpRecyclerViewData() {

        FillHabbitData();


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_list.setLayoutManager(mLayoutManager);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_list.addItemDecoration(itemDecorator);

        ChooseHabitAdapter mChooseHabitAdapter = new ChooseHabitAdapter(mContext, mHabitItems);
        rv_list.setAdapter(mChooseHabitAdapter);
    }

    private void FillHabbitData() {

        mHabitItems.clear();

        switch (APPROACH) {
            case 0:
                mHabitItems.add("Broccoli");
                mHabitItems.add("Adding vegetables for 2 meals in a day");
                mHabitItems.add("One fruit a day");
                mHabitItems.add("Avoiding sugar");
                mHabitItems.add("Avoiding white flour");
                mHabitItems.add("Less red meat");
                mHabitItems.add("Eating pasteur egg");
                mHabitItems.add("Eating pasteur chicken");

                break;

            case 1:
                mHabitItems.add("Brisk walking");
                mHabitItems.add("Brisk walking with weights");
                mHabitItems.add("Skipping");
                mHabitItems.add("Jogging");
                mHabitItems.add("Running");
                mHabitItems.add("Gymming");
                mHabitItems.add("Swimming");
                mHabitItems.add("Dancing");

                break;
            case 2:
                mHabitItems.add("Mindfulness meditation");
                mHabitItems.add("Yoga");
                mHabitItems.add("Reiki");
                mHabitItems.add("Pranic healing");
                mHabitItems.add("Progressive muscle relaxation");

                break;
            case 3:
                mHabitItems.add("Nutrigenetic testing");

                break;
            case 4:
                mHabitItems.add("Castor oil detox");
                mHabitItems.add("Water cleansing");
                mHabitItems.add("Triphala detox");
                mHabitItems.add("Fruit fasting");
                mHabitItems.add("Avoiding harsh chemicals in shampoo, bath soap");
                mHabitItems.add("Avoiding fluoride from toothpaste, water");


                break;
            case 5:
                mHabitItems.add("Prayer");
                mHabitItems.add("Gratitude");

                break;
        }
    }
}
