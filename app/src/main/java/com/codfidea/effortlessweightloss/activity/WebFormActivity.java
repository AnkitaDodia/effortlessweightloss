package com.codfidea.effortlessweightloss.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.codfidea.effortlessweightloss.R;

public class WebFormActivity extends AppCompatActivity {

    Context mContext;
    WebView wv_form;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_form);

        mContext = this;


        setUpToolbar();
        initView();
    }

    private void setUpToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.getNavigationIcon().setColorFilter(getResources().getColor( R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setTitle("Health History Forms");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
    }

    private void initView() {

        wv_form = findViewById(R.id.wv_form);
        wv_form.setWebViewClient(new AppWebViewClients());
        wv_form.getSettings().setJavaScriptEnabled(true);
        wv_form.getSettings().setUseWideViewPort(true);
        wv_form.loadUrl("https://docs.google.com/document/d/e/2PACX-1vQCzuEEto0Nc8qjETE_fpfzUcycXGKxF_RQ6DsEJx034C4tmrCArYNzsHqop0KOLV7hWuOSLmUVFaQy/pub");

    }

    public class AppWebViewClients extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            startActivity(new Intent(mContext, ChooseApproachActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
