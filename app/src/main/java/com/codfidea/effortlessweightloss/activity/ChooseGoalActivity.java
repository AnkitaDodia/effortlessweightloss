package com.codfidea.effortlessweightloss.activity;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.adapter.ChooseGoalAdapter;
import com.codfidea.effortlessweightloss.common.BaseActivity;

import java.util.ArrayList;

public class ChooseGoalActivity extends BaseActivity {

    ChooseGoalActivity mContext;

    private Toolbar mToolbar;
    private CardView card_goal_label;
    TextView mTxtLabelChooseGoal;

    RecyclerView rv_list;

    ArrayList<String> mStringItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_goal);

        mContext = this;

        setUpToolbar();
        initView();
        setUpRecyclerViewData();
    }

    private void setUpToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setTitle("Choose Your Health Goal");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
    }

    private void initView() {
        card_goal_label = findViewById(R.id.card_goal_label);
        card_goal_label.setVisibility(View.VISIBLE);
        mTxtLabelChooseGoal = findViewById(R.id.txt_label_choose_goal);
        mTxtLabelChooseGoal.setTypeface(getRegularFonts(mContext));

        rv_list = findViewById(R.id.rv_list);

    }

    private void setUpRecyclerViewData() {
        mStringItems.add("Obesity");
        mStringItems.add("Thyroid dysfunction");
        mStringItems.add("Stress");
        mStringItems.add("Depression");
        mStringItems.add("Irregular menstrual cycle");
        mStringItems.add("Uterine fibroids");
        mStringItems.add("PCOD");
        mStringItems.add("Blood pressure");
        mStringItems.add("Diabetes");
        mStringItems.add("Smoking habit");
        mStringItems.add("Unhealthy eating");

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_list.setLayoutManager(mLayoutManager);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_list.addItemDecoration(itemDecorator);

        ChooseGoalAdapter mGoalAdapter = new ChooseGoalAdapter(mContext, mStringItems);
        rv_list.setAdapter(mGoalAdapter);
    }
}
