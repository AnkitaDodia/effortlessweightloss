package com.codfidea.effortlessweightloss.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.common.BaseActivity;

public class SignUpActivity extends BaseActivity {

    Context mContext;
    Button btn_guest;
    TextView txt_title, text_tagline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mContext = this;

        btn_guest = findViewById(R.id.btn_guest);

        txt_title = findViewById(R.id.txt_title);
        text_tagline = findViewById(R.id.text_tagline);

        txt_title.setTypeface(getRegularFonts(mContext));
        text_tagline.setTypeface(getMediumFonts(mContext));

        btn_guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(mContext, DashBoardActivity.class));
            }
        });

    }
}
