package com.codfidea.effortlessweightloss.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.activity.ChooseGoalActivity;
import com.codfidea.effortlessweightloss.activity.HealthHistoryActivity;

import java.util.ArrayList;

public class ChooseGoalAdapter extends RecyclerView.Adapter<ChooseGoalAdapter.MyViewHolder>{
    ArrayList<String> mList = new ArrayList<>();

    ChooseGoalActivity mContext;

    public ChooseGoalAdapter(ChooseGoalActivity mContext, ArrayList<String> mList)
    {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ChooseGoalAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item_choose_goal, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChooseGoalAdapter.MyViewHolder holder, int i) {
        String str = mList.get(i);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.layout_row_main.setLayoutParams(params);

        holder.text_item_goal.setText(str);
        holder.text_item_goal.setTypeface(mContext.getRegularFonts(mContext));

        holder.layout_row_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, HealthHistoryActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView text_item_goal;

        LinearLayout layout_row_main;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            layout_row_main = itemView.findViewById(R.id.layout_row_main);

            text_item_goal = itemView.findViewById(R.id.text_item_goal);
        }

    }
}
