package com.codfidea.effortlessweightloss.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.activity.DashBoardActivity;
import com.codfidea.effortlessweightloss.activity.IntroScreenActivity;
import com.codfidea.effortlessweightloss.activity.SignUpActivity;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends PagerAdapter {

    private IntroScreenActivity context;

    private ArrayList<Integer> image;
    private ArrayList<String> text;


    public SliderAdapter(IntroScreenActivity context, ArrayList<Integer> image, ArrayList<String> text) {
        this.context = context;
        this.image = image;
        this.text = text;
    }

    @Override
    public int getCount() {
        return image.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = null;
        if (position == 2) {

            view = inflater.inflate(R.layout.item_slider_video, null);

            VideoView mVideoView = view.findViewById(R.id.videoView);

            TextView text_slider_video = view.findViewById(R.id.text_slider_video);
            text_slider_video.setTypeface(context.getMediumFonts(context));

            String uriPath = "android.resource://" + context.getPackageName() + "/" + image.get(2);
            Uri uri = Uri.parse(uriPath);
            mVideoView.setVideoURI(uri);
            mVideoView.requestFocus();
            mVideoView.start();

            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
//                    Toast.makeText(context, "Finish", Toast.LENGTH_LONG).show();
                }
            });

        } else {
            view = inflater.inflate(R.layout.item_slider_image, null);

            ImageView img_slider = view.findViewById(R.id.img_slider);
            TextView text_slider_text = view.findViewById(R.id.text_slider_text);
            TextView txt_welcome = view.findViewById(R.id.txt_welcome);


            Button btn_ready = view.findViewById(R.id.btn_ready);

            txt_welcome.setTypeface(context.getBoldFonts(context));
            text_slider_text.setTypeface(context.getMediumFonts(context));

            img_slider.setImageResource(image.get(position));
            text_slider_text.setText(text.get(position));

            if(position == 3){
                btn_ready.setVisibility(View.VISIBLE);

            }else {
                btn_ready.setVisibility(View.GONE);

            }

            if(position == 0){
                txt_welcome.setVisibility(View.VISIBLE);
            }else {
                txt_welcome.setVisibility(View.GONE);

            }

            btn_ready.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context,SignUpActivity.class));
                }
            });


        }

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}