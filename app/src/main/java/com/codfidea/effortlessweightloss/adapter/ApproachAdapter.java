package com.codfidea.effortlessweightloss.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.activity.ChooseApproachActivity;
import com.codfidea.effortlessweightloss.activity.ChooseGoalActivity;
import com.codfidea.effortlessweightloss.activity.ChooseHabitActivity;
import com.codfidea.effortlessweightloss.common.BaseActivity;

import java.util.ArrayList;


public class ApproachAdapter extends RecyclerView.Adapter<ApproachAdapter.MyViewHolder>
{
    ArrayList<String> mStringItems = new ArrayList<>();
    ArrayList<Integer> mImagesItems = new ArrayList<>();
    ChooseApproachActivity mContext;


    public ApproachAdapter(ChooseApproachActivity mContext, ArrayList<String> mList, ArrayList<Integer> mImgList)
    {
        this.mContext = mContext;
        this.mStringItems = mList;
        this.mImagesItems = mImgList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_approach_grid, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.txt_approach_title.setText(mStringItems.get(position));
        holder.img_approach_item.setImageResource(mImagesItems.get(position));
        holder.txt_approach_title.setTypeface(mContext.getMediumFonts(mContext));

        holder.img_approach_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.APPROACH = position ;
                mContext.startActivity(new Intent(mContext, ChooseHabitActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mStringItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_approach_title;
        ImageView img_approach_item;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            txt_approach_title = itemView.findViewById(R.id.txt_approach_title);
            img_approach_item = itemView.findViewById(R.id.img_approach_item);

        }

    }
}
