package com.codfidea.effortlessweightloss.adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.activity.ChooseHabitActivity;
import com.codfidea.effortlessweightloss.activity.DashBoardActivity;
import com.codfidea.effortlessweightloss.common.BaseActivity;

import java.util.ArrayList;

public class ChooseHabitAdapter extends RecyclerView.Adapter<ChooseHabitAdapter.MyViewHolder>{

    ArrayList<String> mHabitList = new ArrayList<>();
    ChooseHabitActivity mContext;

    public ChooseHabitAdapter(ChooseHabitActivity mContext, ArrayList<String> mList)
    {
        this.mContext = mContext;
        this.mHabitList = mList;
    }

    @NonNull
    @Override
    public ChooseHabitAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item_choose_goal, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChooseHabitAdapter.MyViewHolder holder, int i) {
        final String str = mHabitList.get(i);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.layout_row_main.setLayoutParams(params);

        holder.text_item_goal.setText(str);
        holder.text_item_goal.setTypeface(mContext.getRegularFonts(mContext));

        holder.layout_row_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                StringBuilder sb = new StringBuilder();
//                for (int i = 0; i < playlists.length; i++) {
//                    sb.append(playlists[i]).append(",");
//                }
//                mStoredHabitList = playlist.split(",");

                BaseActivity.mStoredHabitList.add(str);
                mContext.startActivity(new Intent(mContext, DashBoardActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mHabitList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView text_item_goal;

        LinearLayout layout_row_main;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            layout_row_main = itemView.findViewById(R.id.layout_row_main);

            text_item_goal = itemView.findViewById(R.id.text_item_goal);
        }

    }
}
