package com.codfidea.effortlessweightloss.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;

import com.codfidea.effortlessweightloss.utility.TypeFaces;

import java.util.ArrayList;

public class BaseActivity extends AppCompatActivity {

    public static int APPROACH = 0;
    public static ArrayList<String> mStoredHabitList = new ArrayList<>();

    public Typeface getBoldFonts(Context mContext)
    {
        Typeface font = TypeFaces.getTypeFace(mContext, "Poppins-Bold.ttf");
        return font;
    }
    public Typeface getRegularFonts(Context mContext)
    {
        Typeface font = TypeFaces.getTypeFace(mContext, "Poppins-SemiBold.ttf");
        return font;
    }
    public Typeface getMediumFonts(Context mContext)
    {
        Typeface font = TypeFaces.getTypeFace(mContext, "Poppins-Medium.ttf");
        return font;
    }
    public Typeface getRegularItalicFonts(Context mContext)
    {
        Typeface font = TypeFaces.getTypeFace(mContext, "Poppins-SemiBoldItalic.ttf");
        return font;
    }

    public void setHabits(String stng)
    {
        SharedPreferences sp = getSharedPreferences("HABIT",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("habits",stng);
        spe.apply();
    }

    public String getHabits()
    {
        SharedPreferences sp = getSharedPreferences("HABIT",MODE_PRIVATE);
        String stng = sp.getString("habits",null);
        return stng;
    }


}
