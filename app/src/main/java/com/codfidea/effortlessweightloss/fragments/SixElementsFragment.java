package com.codfidea.effortlessweightloss.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.activity.DashBoardActivity;


public class SixElementsFragment extends Fragment {

    DashBoardActivity mContext;
    TextView txt_label_six_elements_que, txt_label_six_elements_ans;

    public SixElementsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sixelements, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity) getActivity();

        initViews(view);
        SetClickListner();


    }

    private void initViews(View view) {

        txt_label_six_elements_que = view.findViewById(R.id.txt_label_six_elements_que);
        txt_label_six_elements_ans = view.findViewById(R.id.txt_label_six_elements_ans);

//        Spanned sp = Html.fromHtml( getString(R.string.six_elements_html));
//        txt_label_six_elements_ans.setText(sp);

        txt_label_six_elements_que.setTypeface(mContext.getRegularFonts(mContext));
        txt_label_six_elements_ans.setTypeface(mContext.getMediumFonts(mContext));

    }

    private void SetClickListner(){


    }

}
