package com.codfidea.effortlessweightloss.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.activity.DashBoardActivity;


public class ProfileFragment extends Fragment {

    DashBoardActivity mContext;
    TextView txt_label_msg, txt_label_title;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity) getActivity();

        initViews(view);
        SetClickListner();


    }

    private void initViews(View view) {

        txt_label_msg = view.findViewById(R.id.txt_label_msg);
        txt_label_title = view.findViewById(R.id.txt_label_title);

        txt_label_msg.setTypeface(mContext.getRegularItalicFonts(mContext));
        txt_label_title.setTypeface(mContext.getRegularFonts(mContext));

    }

    private void SetClickListner(){


    }

}
