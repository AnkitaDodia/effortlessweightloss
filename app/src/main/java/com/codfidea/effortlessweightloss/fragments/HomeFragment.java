package com.codfidea.effortlessweightloss.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codfidea.effortlessweightloss.R;
import com.codfidea.effortlessweightloss.activity.ChooseGoalActivity;
import com.codfidea.effortlessweightloss.activity.DashBoardActivity;
import com.codfidea.effortlessweightloss.adapter.ChooseHomeHabitAdapter;
import com.codfidea.effortlessweightloss.common.BaseActivity;


public class HomeFragment extends Fragment {

    DashBoardActivity mContext;
    TextView txt_label_msg, txt_label_name, txt_label_add_goal;
    LinearLayout ll_parent_home, ll_add_goal;

    RecyclerView rv_habitlist;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity)getActivity();

        initViews(view);
        SetClickListner();

    }

    private void initViews(View view) {

        txt_label_msg = view.findViewById(R.id.txt_label_msg);
        txt_label_name = view.findViewById(R.id.txt_label_name);
        txt_label_add_goal = view.findViewById(R.id.txt_label_add_goal);

        ll_parent_home = view.findViewById(R.id.ll_parent_home);
        ll_add_goal = view.findViewById(R.id.ll_add_goal);


        txt_label_name.setTypeface(mContext.getMediumFonts(mContext));
        txt_label_msg.setTypeface(mContext.getRegularItalicFonts(mContext));
        txt_label_add_goal.setTypeface(mContext.getRegularFonts(mContext));

        rv_habitlist = view.findViewById(R.id.rv_habitlist);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_habitlist.setLayoutManager(mLayoutManager);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_habitlist.addItemDecoration(itemDecorator);

        ChooseHomeHabitAdapter mChooseHomeHabitAdapter = new ChooseHomeHabitAdapter(mContext, BaseActivity.mStoredHabitList);
        rv_habitlist.setAdapter(mChooseHomeHabitAdapter);

    }

    private void SetClickListner(){

        ll_add_goal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(mContext, ChooseGoalActivity.class));
            }
        });

    }

}
